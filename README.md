# Straffeloven 2005

## § 111. Krenkelse av Norges selvstendighet og fred

Med fengsel inntil 15 år straffes den som ved bruk av makt, trusler eller på annen rettsstridig måte volder fare for at Norge eller en del av Norge
a)      innlemmes i en annen stat,
b)      blir underlagt en fremmed stats herredømme,
c)      eller en stat som Norge er alliert eller i kampfellesskap med, blir påført krig eller fiendtligheter,
d)      påføres vesentlige begrensninger i sin selvbestemmelsesrett, eller
e)      løsrives.

## § 204. Innbrudd i datasystem

Med bot eller fengsel inntil 2 år straffes den som ved å bryte en beskyttelse eller ved annen uberettiget fremgangsmåte skaffer seg tilgang til datasystem eller del av det.
## § 205. Krenkelse av retten til privat kommunikasjon

Med bot eller fengsel inntil 2 år straffes den som uberettiget
a)	og ved bruk av teknisk hjelpemiddel hemmelig avlytter eller gjør hemmelig opptak av telefonsamtale eller annen kommunikasjon mellom andre, eller av forhandlinger i lukket møte som han ikke selv deltar i, eller som han uberettiget har skaffet seg tilgang til,
b)	bryter en beskyttelse eller på annen uberettiget måte skaffer seg tilgang til informasjon som overføres ved elektroniske eller andre tekniske hjelpemidler,
c)	åpner brev eller annen lukket skriftlig meddelelse som er adressert til en annen, eller på annen måte skaffer seg uberettiget tilgang til innholdet, eller
d)	hindrer eller forsinker adressatens mottak av en meddelelse ved å skjule, endre, forvanske, ødelegge eller holde meddelelsen tilbake.

## § 206. Fare for driftshindring

Med bot eller fengsel inntil 2 år straffes den som ved å overføre, skade, slette, forringe, endre, tilføye eller fjerne informasjon uberettiget volder fare for avbrudd eller vesentlig hindring av driften av et datasystem.
